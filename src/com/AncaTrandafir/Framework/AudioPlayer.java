package com.AncaTrandafir.Framework;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

import java.util.HashMap;
import java.util.Map;

public class AudioPlayer {

    public static Map<String, Sound> soundMap = new HashMap<String, Sound>();
    public static Map<String, Music> musicMap = new HashMap<String, Music>();

    public boolean playing = false;  // daca e pornit volumul

    public void load() {     // o vom intializa o singura data pt a crea AudioPlayer-ul si o vom chema direct din clasa; de-asta e statica

        // load all the sounds
        try {
            musicMap.put("music", new Music("res/tinkle.wav"));
            soundMap.put("menuSound", new Sound("res/buttonPush.wav"));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }


    // getteri
    public static Music getMusic(String key) {
        return musicMap.get(key);
    }

        public static Sound getSound(String key){
            return soundMap.get(key);
    }


}

