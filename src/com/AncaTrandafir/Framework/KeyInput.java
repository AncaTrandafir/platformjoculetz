package com.AncaTrandafir.Framework;

import com.AncaTrandafir.Game;
import com.AncaTrandafir.Object.BulletPlayer;
import com.AncaTrandafir.Window.Camera;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

    private Game game;
    private Handler handler;
    private Texture texture;

    // sa nu se mai blocheze tastele cand ne miscam f repede
    // cream un vector de boolean pt a memora daca este apasata tasta
    private boolean[] keyDown = new boolean[2];

    public KeyInput(Game game, Handler handler, Texture texture) {       // transmitem lista de obiecte ca parametru

        this.game = game;       // pt a putea accesa starile: shop, game etc
        this.handler = handler;
        this.texture = texture; // pt a putea incarca imaginea cu bullet din texture


        keyDown[0] = false;     // initial A-D nu sunt apasate
        keyDown[1] = false;

    }



    /**
     * Notification from AWT that a key has been pressed. Note that
     * a key being pressed is equal to being pushed down but *NOT*
     * released.
     *
     * @param e The details of the key that was pressed
     */

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();  // key-binding, shows number value corresponding of the pressed key; sau alta metoda sa lucram cu ASCII cod



        // SETAM TASTE DIFERITE PT PLAYERI DIFERITI  -------------------------
//        for (int i = 0; i < handler.objectList.size(); i++) {    // traversam lista
//            GameObject tempObj = handler.objectList.get(i);
//
//            if (tempObj.getId() == ID.Player)     // daca id-ul obiectului din handler este id de player1 sau player2
//                if (key == KeyEvent.VK_W) {        // daca keyEvent-ul are value key de W
//                    tempObj.setY(tempObj.getY() - 1); // decrementeaza Y cu o pozitie, adica urca un pas
//                }
//
//            if (tempObj.getId() == ID.Player2) // key event pt Player 2 - alta tasta
//                if (key == KeyEvent.VK_UP)         // daca keyEvent-ul are value key UP
//                    tempObj.setY(tempObj.getY() - 1);
//        } -------------------------------------------------------------------



        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.Player) {

                if (key == KeyEvent.VK_D) {
                    tempObject.setVelX(5);        // se deplaseaza doar stanga-dreapta, deci doar velX
                    keyDown[0] = true;              // true pt ca se apasa tasta
                }

                if (key == KeyEvent.VK_A) {
                    tempObject.setVelX(-5);
                    keyDown[1] = true;
                }

                if (key == KeyEvent.VK_W && !tempObject.isJumping()) {          // W for jump
                    // sa nu sarim la nesfarsit in sus ca si cum am zbura, ci doar o data
                    // treb sa atinga pamantul inainte sa mai poata sari o data
                    tempObject.setJumping(true);
                    tempObject.setVelY(-5);
                }

                if (key == KeyEvent.VK_SPACE) {              // getFacing este pt orientare

                       if (tempObject.getFacing() > 0)              // diferenta 96 si 32 este intre pozitia glontului, relativa fata de player
                             handler.addObject(new BulletPlayer(tempObject.getX() + 96, tempObject.getY() + 10,  ID.BulletPLayer, tempObject.getFacing() * 5, texture));
                        else
                            handler.addObject(new BulletPlayer(tempObject.getX() - 32, tempObject.getY() + 10,  ID.BulletPLayer, tempObject.getFacing() * 5, texture));
                }

                }
        }


        if (key == KeyEvent.VK_ESCAPE) System.exit(1);      // inchidem fereastra si cu ESC, pe langa click X


        if (key == KeyEvent.VK_P) {                 // pause the game cu tasta P
            if (Game.paused) Game.paused = false;
            else Game.paused = true;
        }

        if (key == KeyEvent.VK_ENTER) {
            if (game.gameState == Game.STATE.Game) {  // doar din starea de joc pot intra in Shop
                game.gameState = Game.STATE.Shop;
            } else if (game.gameState == Game.STATE.Shop) {
                game.gameState = Game.STATE.Game;      // apas iar Enter cand sunt deja in Shop si ma intorc la Game
            }
        }           // in celelalte stari P nu face nimic


    }

    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();  // key-binding, shows number value corresponding of the pressed key; sau alta metoda sa lucram cu ASCII cod

        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.Player) {

                if (key == KeyEvent.VK_D)
                    //tempObject.setVelX(0); // cand eliberam tasta, se opreste; altfel se tot duce :)        setam cu 0 velocitatea, sau setam variabila keyDown = false
                    keyDown[0] = false;

                if (key == KeyEvent.VK_A)
//                    tempObject.setVelX(0);
                    keyDown[1] = false;

                if (!keyDown[0] && !keyDown[1]) tempObject.setVelX(0);
            }
        }
    }



}
