package com.AncaTrandafir.Framework;

public enum ID {        // creates an enumeration and now we can id player or enemy by its id
    Player(),
    Brick(),
    FleaEnemy(),
    CatEnemy(),
    LizardEnemy(),
    BulletPLayer(),
    BulletLizardEnemy(),
    CoinBonus(),
    ChestBonus(),
    Trail(),
    EndGameAnimation(),
    EndLevel();

}
