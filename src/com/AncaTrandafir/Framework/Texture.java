package com.AncaTrandafir.Framework;

import com.AncaTrandafir.Window.BufferedImageLoader;
import com.AncaTrandafir.Window.SpriteSheet;

import java.awt.image.BufferedImage;

public class Texture {

    private SpriteSheet blockSpriteSheet, dogSpriteSheet, catSpriteSheet, lizardSpriteSheet;
    private BufferedImage dog, cat, blocks, lizard;
    public BufferedImage brickImg, grassImg, flowerImg, waterImg, boneImg, foodImg, heartImg, audioImg;
    public BufferedImage idlePlayerRight, idlePlayerLeft, fleaImg, fireBallImg, fleaBallImg, coinChestImg, endLevelImg, idleCatRight, idleCatLeft;

    public BufferedImage[][] matrixPlayerRunRight = new BufferedImage[2][10]; // matrice
    public BufferedImage[][] matrixPlayerRunLeft = new BufferedImage[2][10];
    public BufferedImage[][] matrixPlayerJumpRight = new BufferedImage[2][10];
    public BufferedImage[][] matrixPlayerJumpLeft = new BufferedImage[2][10];
    public BufferedImage[][] catRunLeft = new BufferedImage[1][12];
    public BufferedImage[][] catRunRight = new BufferedImage[1][12];
    public BufferedImage[][] lizardRun = new BufferedImage[1][4];
    public BufferedImage[][] coinSpin = new BufferedImage[1][5];


    public Texture(){

        BufferedImageLoader loader = new BufferedImageLoader();

        blocks = loader.loadImage("res/blocksSpriteSheet.png");
        dog = loader.loadImage("res/dogSpriteSheetLeftRight.png");
        cat = loader.loadImage("res/cats.png");
        lizard = loader.loadImage("res/lizard.png");


        blockSpriteSheet = new SpriteSheet(blocks);
        dogSpriteSheet = new SpriteSheet(dog);
        catSpriteSheet = new SpriteSheet(cat);
        lizardSpriteSheet = new SpriteSheet(lizard);

       getTextures();


    }

    public void getTextures() {

        brickImg = blockSpriteSheet.grabImage(1, 1, 32, 32);
        grassImg = blockSpriteSheet.grabImage(2, 1, 32, 32);
        flowerImg = blockSpriteSheet.grabImage(3, 1, 32, 32);
        waterImg = blockSpriteSheet.grabImage(4, 1, 32, 32);

        fleaImg = blockSpriteSheet.grabImage(1,2, 32, 32);
        fireBallImg = blockSpriteSheet.grabImage(4,2, 32, 32);
        fleaBallImg = blockSpriteSheet.grabImage(5,2, 32, 32);
        boneImg = blockSpriteSheet.grabImage(3,2, 32, 32);
        foodImg = blockSpriteSheet.grabImage(2,2, 64, 64);
        coinChestImg = blockSpriteSheet.grabImage(3,2, 64, 64);

        idlePlayerRight = dogSpriteSheet.grabImage(8,4, 96, 64);      // idle dog orientated right
        idlePlayerLeft = dogSpriteSheet.grabImage(3,8, 96, 64);

        audioImg = blockSpriteSheet.grabImage(6,1, 32, 32);
        heartImg = blockSpriteSheet.grabImage(1,2, 64, 64);
        endLevelImg = blockSpriteSheet.grabImage(1,2, 64, 64);

        // player jumping to the right
        for (int i=0; i<2; i++)          // 2 randuri, 10 coloane
            for (int j=0; j<10; j++) {
                matrixPlayerJumpRight[i][j] = dogSpriteSheet.grabImage(j+1, i+1, 96, 64);    // +1 pt ca numaratoarea in array incepe de la 0, dar indexarea pozelelor de la row1, col1
            }

        // player running to the right
        for (int i=0; i<2; i++)          // rnadurile 3, 4 si 5, 10 coloane             // indexarea porneste mereu de la 0, ajustam separat nr rand si col; ATT: i pt row, j pt col
            for (int j=0; j<10; j++) {
                matrixPlayerRunRight[i][j] = dogSpriteSheet.grabImage(j+1, i + 2, 96, 64);
            }

        // player jumping to the left
        for (int i=0; i<2; i++)          // randurile 6 si 7, 10 coloane
            for (int j=0; j<10; j++) {          // sa ii ia din sheet de la dreapta la stanga ca sunt facuti in oglinda
                matrixPlayerJumpLeft[i][j] = dogSpriteSheet.grabImage(10-j, i + 6, 96, 64);
            }

        // player running to the left
        for (int i=0; i<2; i++)          // rnadurile 8 si 9 si 10, 10 coloane
            for (int j=0; j<10;j++) {
                matrixPlayerRunLeft[i][j] = dogSpriteSheet.grabImage(10-j, i + 8, 96, 64);
            }


        // cat running to the left
        for (int i=0; i<1;i++)
            for (int j=0; j<12;j++)
                catRunLeft[i][j] = catSpriteSheet.grabImage(j+1, i+1, 128, 64);


        // cat running to the right
        for (int i=0; i<1;i++)
            for (int j=0; j<12;j++)
                catRunRight[i][j] = catSpriteSheet.grabImage(j+1, i+2, 128, 64);

        idleCatRight = catSpriteSheet.grabImage(4,2, 96, 64);      // idle dog orientated right
        idleCatLeft = catSpriteSheet.grabImage(9,1, 96, 64);

        // lizard
        for (int i=0; i<1;i++)
            for (int j=0; j<4;j++)
                lizardRun[i][j] = lizardSpriteSheet.grabImage(j+1, i+1, 460, 161);



        // coin spin: row 5
        for (int i=0; i<1;i++)
            for (int j=0; j<5;j++)
                coinSpin[i][j] = blockSpriteSheet.grabImage(j+1, i+5, 32, 32);
    }


}
