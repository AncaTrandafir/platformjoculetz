package com.AncaTrandafir.Framework;

import com.AncaTrandafir.Game;
import com.AncaTrandafir.Object.Block;

import java.awt.*;
import java.util.LinkedList;

public class Handler {      // loop through all of the objects, update them and render them on the screen


    public LinkedList<GameObject> objectList = new LinkedList<GameObject>();
    private GameObject tempGameObject;

    public int speed = 5; // pt a creste viteza cand cumpara viteza; o accesez din cls KeyInput, pt ca acolo am velocitatile setate pt Player.

    public void tick(){     // updates all game objects
        for (int i=0; i<objectList.size(); i++) {   // loop through all objects
            tempGameObject = objectList.get(i);
            tempGameObject.tick(objectList); // primeste parametru un LinkedList
        }
    }

    public void render(Graphics g){       // renders all game objects
        for (int i=0; i<objectList.size(); i++) {   // loop through all objects
            GameObject tempGameObject = objectList.get(i);
            tempGameObject.render(g);
        }
    }

    public void addObject(GameObject object) {
        objectList.add(object);
    }

    public void removeObject(GameObject object) {
        objectList.remove(object);
    }



//    public void createLevel(){        // level creat manual
//        for (int i=0; i < Game.HEIGHT; i += 32)
//            addObject(new Block(0, i, ID.Brick));
//
//        for (int i=0; i < Game.WIDTH * 2; i += 32)
//            addObject(new Block(i, Game.HEIGHT - 85, ID.Brick));
//
//        for (int i = 0; i < Game.WIDTH / 2 ; i += 32)
//            addObject(new Block(250+i, Game.HEIGHT - 250, ID.Brick));
//    }


    public void clearLevel(){           // sterge toate obiectele din handler
        objectList.clear();
    }


    public void clearObjectList() {                         // ????? cumva este nevoie de metodele astea separat pt ca nu resusesc sa reincarc level cand dau play again ??????

        for (int i = 0; i < objectList.size(); i++)
            removeObject(objectList.get(i));
    }


    public void clearEndGameAnimation() {
        for (int i=0; i<objectList.size(); i++)  // golim lista de obiecte tip MenuAnimation + trail
            if (this.objectList.get(i).getId() == ID.EndGameAnimation)
                removeObject(objectList.get(i));
    }



//    public void changeLevel(){
//
//        switch(Game.LEVEL) {
//            case 1:
//                loadImageLevel(level1SpriteSheet);
//                break;
//            case 2:
//                loadImageLevel(level2SpriteSheet);
//                break;
//        }
//    }

}
