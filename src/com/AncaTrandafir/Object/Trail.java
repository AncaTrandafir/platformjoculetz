package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class Trail extends GameObject {

    Handler handler;
    Color color;
    int width, height;
    float life;    // let's say life = 0,001 -> 0.01; the smaller the number, the longer the life of the trail will be


    // the degree of transparency - alpha = 1.0 is totally opaque, alpha = 0.0 totally transparent (clear)
    public float alpha = 1;

    public Trail(int x, int y, ID id, Color color, int width, int height, float life, Handler handler) {
        super(x, y, id);
        this.handler = handler;
        this.color = color;
        this.width = width;
        this.height = height;
        this.life = life;


    }


    @Override
      public void tick(LinkedList<GameObject> objectList) {  // Pt fiecare obiect EndGameAnimation, valoarea alfa descreste treptat, astfel ca apare efectul de fade
        if (alpha > life)
            alpha -= (life - 0.0001f);      // tot descreste alpha, iar cand iese din if dispare cu totul
        else                                // => fade :)
            handler.removeObject(this);     // coada este inlaturata din lista de obiecte; altfel imi scade considerabil FPS si se blocheaza jocul pt ca lista se tot incarca cu obiecte invizibile dar existente de trail

    }




    @Override
    public void render(Graphics g) {
        Graphics2D g2D = (Graphics2D) g; // cast lui g la Graphics2D ca sa putem folosi AlphaComposite
        g2D.setComposite(this.makeTransparent(alpha));
        g.setColor(color);
        g.fillRect((int)x, (int)y, width, height);

        g2D.setComposite(this.makeTransparent(1));  // --> nu stiu exact ce face, dar treb, ca altfel face si alte lucruri transparente, da peste cap..

    }

    @Override
    public Rectangle getBounds() {      // coada nu se intersecteaza deci returneaza null
        return null;
    }


    // AlphaComposite - to achieve blending and transparency effects with graphics
    // Source over - If pixels in the object being rendered (the source) have the same location as previously rendered pixels (the destination),
    //              the source pixels are rendered over the destination pixels.

    private AlphaComposite makeTransparent(float alpha) {
        int type = AlphaComposite.SRC_OVER;
        return AlphaComposite.getInstance(type, alpha);
    }

}
