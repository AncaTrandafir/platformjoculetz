package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.*;

import java.awt.*;
import java.util.LinkedList;

public class Player extends GameObject {

    private float width = 96, height = 64;
    private float gravity = 0.1f; // viteza gravitatiei
    private final float MAX_SPEED = 10;


    // avem nevoie de facing si in cls KeyInput cand apas tasta sa trag cu gloante, si il accesez mai usor cu getFacing direct din cls abstracta GameObject
    // private int facing; // orientarea: 1 spre drepata, -1 spre stanga

    private Handler handler;
    private Texture texture;
    private Camera camera;
    private LevelRGB level;

    private CharacterAnimation playerRunRight, playerJumpRight, playerRunLeft, playerJumpLeft;


    public Player(int x, int y, ID id, Handler handler, Texture texture, Camera camera, LevelRGB level) {           // incarc imagimea mea in constrcutor
        super(x, y, id);
        this.handler = handler;         // injectam lista de obiecte pt ca se va ciocni de ele
        this.texture = texture;         // injectam texture pt ca acolo e imaginea cu player
        this.camera = camera;           // injectam camera ca sa facem render de obiecte care sunt doar in perimetrul camerei; altfel ele se incarca toate si scade simtittor fps
        this.level = level;             // injectam level in player pt ca atunci cand ajunge la final nivelm se schimba nivelul

        facing = 1; // il orientam spre dreapta cand il initializam

        //   level = new LevelRGB(Game.level1SpriteSheet, handler, texture, camera);  // level initial 1

        for (int i = 0; i < 2; i++)
            for (int j=0; j<10; j++)
                playerRunRight = new CharacterAnimation(2, texture.matrixPlayerRunRight[i]);

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 10; j++)
                playerRunLeft = new CharacterAnimation(2, texture.matrixPlayerRunLeft[i]);

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 10; j++)
                playerJumpRight = new CharacterAnimation(2, texture.matrixPlayerJumpRight[i]);

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 10; j++)
                playerJumpLeft = new CharacterAnimation(2, texture.matrixPlayerJumpLeft[i]);

    }


    @Override
    public void tick(LinkedList<GameObject> objectList) {

        // conditie pt exteriorul camerei
        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
            ;
        } else {

            // facem tick doar de ce e in interiorul camerei
            x += velX;
            y += velY;


            if (velX > 0) facing = 1;  // setam orientarea player-ului
            else if (velX < 0) facing = -1;             // cazul 0 tratam separat


            if (falling || jumping) { // daca sare sau cade intervine gravitatia;
                velY += gravity;            // pe masura ce cade mai mult, viteza de cadere creste
                if (velY > MAX_SPEED)
                    velY = MAX_SPEED;       // gen clamp, sa nu depaseasca o valoare max.
            }

            collision(objectList);

            // animatiile

            playerRunRight.runAnimation();
            playerRunLeft.runAnimation();
            playerJumpRight.runAnimation();
            playerJumpLeft.runAnimation();


            if (y > Game.HEIGHT) // cade in abis, moare
                HUD.HEALTH = 0;
        }

    }


    @Override
    public void render(Graphics g) {

        // testam sa ne aflam in perimetrul camerei, asta e exteriorul, apoi else
        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
            ;
        } else {

//        g.setColor(Color.WHITE);
//        g.fillRect((int) x, (int) y, (int) width, (int) height);


            // draw player

            if (velX != 0) {
                if (facing == 1) {
                    if (!jumping)       // merge spre dreapta
                        playerRunRight.drawAnimamation(g, (int) x, (int) y);
                    else if (jumping)  // sare spre dreapta dar si cade inapoi; daca nu specific caderea nu are imagine pt cadere si dispare
                        playerJumpRight.drawAnimamation(g, (int) x, (int) y);
                    else if (falling)  // cade cu fata spre dreapta
                        playerJumpRight.drawAnimamation(g, (int) x, (int) y);
                } else if (facing == -1) {
                    if (!jumping)      // merge spre stanga
                        playerRunLeft.drawAnimamation(g, (int) x, (int) y);
                    else if (jumping)  // sare spre stanga
                        playerJumpLeft.drawAnimamation(g, (int) x, (int) y);
                    else if (falling)  // cade cu fata spre stanga
                        playerJumpLeft.drawAnimamation(g, (int) x, (int) y);
                }

            } else if (velX == 0) {
                if (facing == 1) {     // sta pe loc, orientat spre dreapta
                    g.drawImage(texture.idlePlayerRight, (int) x, (int) y, null);
                } else if (facing == -1) {    // // sta pe loc, orientat spre dreapta
                    g.drawImage(texture.idlePlayerLeft, (int) x, (int) y, null);
                }
            }


            texture.getTextures();


//        Graphics2D g2D = (Graphics2D) g;
//        g2D.setColor(Color.PINK);
//        g2D.draw(getBounds());
//        g2D.draw(getBoundsBottom());        // desenez dreptunghiurile de coliziune pt o mai buna vizualizare
//        g2D.draw(getBoundsLeft());
//        g2D.draw(getBoundsRight());
        }

    }


    // facem coliziune pt toate cele 4 parti ale player-ului, construind cate un dreptunghi de bounds in interior

    // valorile sunt calculate vizual, din aproape in aproape

    public Rectangle getBoundsRight() {
        return new Rectangle((int) x + (int) width - 5, (int) y + 5, (int) 5, (int) height - 10);
    }

    public Rectangle getBoundsLeft() {
        return new Rectangle((int) x, (int) y + 5, (int) 5, (int) height - 10);
    }

    public Rectangle getBoundsUpper() {
        return new Rectangle((int) x + (int) width / 2 - (int) (width / 2) / 2, (int) y, (int) width / 2, (int) height / 2);
    }

    public Rectangle getBounds() { // va fi upper bound, treb sa aiba metoda getBounds() mostenita de la parinte
        return new Rectangle((int) x + (int) width / 2 - (int) (width / 2) / 2, (int) y + (int) height / 2, (int) width / 2, (int) height / 2);
    }


    private void collision(LinkedList<GameObject> list) {   // ia ca parametru o lista de obiecte de care se loveste

        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.Brick) {        // daca ne ciocnim de caramida

                // Bottom collision -> getBounds e pt bottom
                if (getBounds().intersects(tempObject.getBounds())) {

                    //  y = tempObject.getY() - height; // fara linia asta de cod Player-ul se scufunda putin in pamant

                    velY = 0;           // -> se opreste din cadere
                    falling = false;    // nu mai cade, nu mai are gravitatie
                    jumping = false;
                } else {  // daca nu ciocneste nimic, sa nu sara la nesfarsit in neant, ci sa inervina gravitatia, adica falling = true;
                    falling = true;

                }

                // Top collision
                if (getBoundsUpper().intersects(tempObject.getBounds())) {      // tempObject este brick, getBoundsUpper este dreptunghiul meu de coliziunse sus

                    y = tempObject.getY() + 34;   // relativ fata de brick
                    velY = 0;

                }

                // Right collision
                if (getBoundsRight().intersects(tempObject.getBounds())) {

                    x = tempObject.getX() - width;
                    //  velX = 0;           // -> se opreste, nu mai inainteaza stanga-dreapta
                }

                // Left collision
                if (getBoundsLeft().intersects(tempObject.getBounds())) {

                    x = tempObject.getX() + 32;
                    //  velX = 0;   // -> nu e nevoie se setam velocitatea la 0;

                }

            } else if (tempObject.getId() == ID.EndLevel) {           // switch level
                if (getBounds().intersects(tempObject.getBounds())) {     // cu inima
                    Game.LEVEL++;
                    level.changeLevel();
                }

            } else if (tempObject.getId() == ID.CatEnemy || tempObject.getId() == ID.FleaEnemy || tempObject.getId() == ID.LizardEnemy || tempObject.getId() == ID.BulletLizardEnemy) {           // coliziuni cu inamici
                if (getBounds().intersects(tempObject.getBounds()))
                    HUD.HEALTH--;       // scade viata

            } else if (tempObject.getId() == ID.CoinBonus) {           // coliziuni cu coin
                if (getBounds().intersects(tempObject.getBounds())) {
                    HUD.SCORE += 50;
                    list.remove(tempObject);  // ia moneda si dispare
                }

            } else if (tempObject.getId() == ID.ChestBonus) {           // coliziuni cufar
                if (getBounds().intersects(tempObject.getBounds())) {
                        HUD.SCORE += 150;
                        list.remove(tempObject); // inlaturam cufarul
            }
        }


        }
    }
}
