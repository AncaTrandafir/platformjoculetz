package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;
import com.AncaTrandafir.Window.CharacterAnimation;
import com.AncaTrandafir.Window.HUDEnemy;

import java.awt.*;
import java.util.LinkedList;
import java.util.Random;

public class LizardEnemy extends GameObject {

    private float width = 460, height = 161;


    private Texture texture;
    private Camera camera;
    private Handler handler;

    private GameObject player;

    private CharacterAnimation lizardRunCharacterAnimation;

    private HUDEnemy hudEnemy;


    public LizardEnemy(int x, int y, ID id, HUDEnemy hudEnemy, Texture texture, Camera camera, Handler handler) {           // incarc tipul si textura in constructor, textura returneaza efectiv imaginea blocului
        super(x, y, id);

        this.texture = texture;
        this.camera = camera;
        this.handler = handler;
        this.hudEnemy = hudEnemy;


        for (int i = 0; i < 1; i++)
            for (int j = 0; j < 4; j++)
                lizardRunCharacterAnimation = new CharacterAnimation(4, texture.lizardRun[i]);


        velX = 2;


    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {


        //conditie pt exteriorul camerei
        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
            ;
        } else {

            // facem tick doar de ce e in interiorul camerei;

            for (int i = 0; i < objectList.size(); i++)
                if (objectList.get(i).getId() == ID.Player)
                    player = objectList.get(i);


            x += velX;

            float diffX = player.getX() - x;         //  merge relativ fata de player, il urmeaza

            float distance = (float) Math.sqrt((x - player.getX()) * (x - player.getX()) + (y - player.getY()) * (y - player.getY()));

            velX = (float) ((-1.3 / distance) * diffX);
            if (x <= 0 || x >= Game.WIDTH - 16)
                velX *= -1;   // merge relativ fata de x-ul game-ului, stanga dreapta, face bounce cand atinge capetele ferestrei; nu folosim camera ca nu e statica


            lizardRunCharacterAnimation.runAnimation();


            Random random = new Random();

            int spawn = random.nextInt(150); // returns a value between 0 and 150
            // Cu cat limita pt random e mai mare cu atat scade probabilitate sa aleaga 0
            // Iar cand alege 0, creeaza un obiect nou;
            // Asa stabilim frecventa gloantelor, care este random
            if (spawn == 0)
                handler.addObject(new BulletLizardEnemy((int) x, (int) y, -2, ID.BulletLizardEnemy, handler, texture));       // velocitate negativa sa mearga inspre stanga


            hudEnemy.tick(); // apelam bara de viata pt Lizard

            collision();  // apelam met de collision pt atunci cand il ating gloantele Plyer-ului

        }
    }


    @Override
    public void render(Graphics g) {

        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
            ;
        } else {

            lizardRunCharacterAnimation.drawAnimamation(g, (int) x, (int) y);

            texture.getTextures();

            hudEnemy.render(g);     // desenam bara de sanatate pt Lizard

        }

    }



    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, (int) width, (int) height);     // sunt dimensiunile caramidei;
        // la fel setam si pt player cu care ne vom intersecta
    }


    private void collision() {   // ia ca parametru o lista de obiecte de care se loveste

        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.BulletPLayer) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    HUDEnemy.healthLizard--;     // daca este atins de glontul player-ului descreste viata lizard-ului
                }
            }
        }

    }



}
