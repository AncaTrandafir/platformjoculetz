package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;

import java.awt.*;
import java.util.LinkedList;

public class EndLevel extends GameObject {

    private float width = 32, height = 32;

    private  Texture texture;

    public EndLevel(int x, int y, ID id, Texture texture) {
        super(x, y, id);
        this.texture = texture;

    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {

    }

    @Override
    public void render(Graphics g) {

//     g.setColor(Color.GREEN);
//     g.fillRect((int)x, (int)y, 64, 64);
//     System.out.println("verdeeeeeeeeeeeeeeeeeeeeeeeeeeee");

        g.drawImage(texture.endLevelImg, (int)x, (int)y , null);

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, (int)width, (int)height);     // sunt dimensiunile caramidei;
                                                              // la fel setam si pt player cu care ne vom intersecta
    }




}
