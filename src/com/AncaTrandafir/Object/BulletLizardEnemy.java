package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Random;

public class BulletLizardEnemy extends GameObject {

    private Handler handler;
    private Trail trail;
    private Texture texture;
    private int velX;


    public BulletLizardEnemy(int x, int y, int velX, ID id, Handler handler, Texture texture) {
        super(x, y, id);
        this.velX = velX;
        this.handler = handler;
        this.texture = texture;


    }

    @Override
    public void tick(LinkedList<GameObject> objectList) {
        x += velX;
        collision();  // apelam met de collision pt atunci cand il ating gloantele Plyer-ului

        if (x <= x - 200) handler.removeObject(this); // zicem ca are o bataie de 200, apoi dispare; il stergem din lista de obiecte

    //    handler.addObject(new Trail((int)x, (int)y, ID.Trail, new Color(235, 63, 29), 8, 8, 0.08f, handler));



    }


    @Override
    public void render(Graphics g) {
//        g.setColor(new Color(235, 63, 29));
//        g.fillRect((int)x, (int)y, 8, 8 );
        g.drawImage(texture.fleaBallImg, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {              // returneaza un dreptunghi de dimensiunile astea
        return new Rectangle((int)x, (int)y, 40, 40);
    }  // fac bounds mai mare sa le poata ochi mai usor



    private void collision() {   // ia ca parametru o lista de obiecte de care se loveste

        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.BulletPLayer) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    handler.objectList.remove(this);     // daca este atins de glontul player-ului dispare obiectul
                }
            }
        }

    }


}
