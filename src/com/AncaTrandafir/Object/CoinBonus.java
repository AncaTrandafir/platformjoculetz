package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;
import com.AncaTrandafir.Window.CharacterAnimation;

import java.awt.*;
import java.util.LinkedList;

public class CoinBonus extends GameObject {

    private float width = 32, height = 32;

    private Texture texture;
    private Camera camera;
    private CharacterAnimation coinSpinAnimation;


    public CoinBonus(int x, int y, ID id, Texture texture, Camera camera) {           // incarc tipul si textura in constructor, textura returneaza efectiv imaginea blocului
        super(x, y, id);
        this.texture = texture;
        this.camera = camera;


        for (int i=0; i<1; i++)
            for (int j=0; j<5; j++)
                coinSpinAnimation = new CharacterAnimation(1, texture.coinSpin[i]);

    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {
        coinSpinAnimation.runAnimation();
    }


    @Override
    public void render(Graphics g) {

          if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
                ; } else {

                coinSpinAnimation.drawAnimamation(g, (int) x, (int) y);


        }

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, (int)width, (int)height);
                                                              // la fel setam si pt player cu care ne vom intersecta
    }


}
