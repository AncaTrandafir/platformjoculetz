package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Random;

public class Block extends GameObject {

    private float width = 32, height = 32;

    private int type;   // introducem o variabila in cosntructor pt a diferentia tipurile de blocuri

    private Texture texture;
    private Camera camera;


    public Block(int x, int y, int type, ID id, Texture texture, Camera camera) {           // incarc tipul si textura in constructor, textura returneaza efectiv imaginea blocului
        super(x, y, id);
        this.type = type;
        this.texture = texture;
        this.camera = camera;

    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {

    }

    @Override
    public void render(Graphics g) {

       // if ((this.x > camera.getX() + Game.WIDTH) && (this.x < camera.getX() - 32) && (this.y > camera.getY() + Game.HEIGHT) && (this.y < camera.getY() - 32)) {
        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
                ; } else {

            if (type == 0)   // diferentiem tipul cu culoarea din sheet-ul de level
                g.drawImage(texture.brickImg, (int) x, (int) y, null);

            if (type == 1)
                g.drawImage(texture.grassImg, (int) x, (int) y, null);

            if (type == 2)
                g.drawImage(texture.flowerImg, (int) x, (int) y, null);

            if (type == 4)
                g.drawImage(texture.waterImg, (int) x, (int) y, null);
        }

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, (int)width, (int)height);     // sunt dimensiunile caramidei;
                                                              // la fel setam si pt player cu care ne vom intersecta
    }




}
