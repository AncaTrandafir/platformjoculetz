package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;

import java.awt.*;
import java.util.LinkedList;

public class FleaEnemy extends GameObject {

    private float width = 32, height = 32;

    private Texture texture;
    private Camera camera;


    public FleaEnemy(int x, int y, ID id, Texture texture, Camera camera) {           // incarc tipul si textura in constructor, textura returneaza efectiv imaginea blocului
        super(x, y, id);
        this.texture = texture;
        this.camera = camera;

        velY = 5;

    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {

        // conditie pt exteriorul camerei
        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
            ; } else {

            // facem tick doar de ce e in interiorul camerei;

            y += velY;

          if (y <= 100 || y >= Game.HEIGHT - 50) velY *= -1;   // urca pana la un sfert din inaltimea ecranului si se intoarce, face bounce
        }
    }



    @Override
    public void render(Graphics g) {

         if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
                ; } else {

            g.drawImage(texture.fleaImg,(int)x, (int)y, null);

        }

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, (int)width, (int)height);     // sunt dimensiunile caramidei;
                                                              // la fel setam si pt player cu care ne vom intersecta
    }




}
