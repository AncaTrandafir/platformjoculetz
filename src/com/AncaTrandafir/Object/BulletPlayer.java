package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;

import java.awt.*;
import java.util.LinkedList;

public class BulletPlayer extends GameObject {

    int velX;
    private Texture texture;
    private Camera camera;

    public BulletPlayer(float x, float y, ID id, int velX, Texture texture) {
        super(x, y, id);
        this.velX = velX;
        this.texture = texture;

    }

    @Override
    public void tick(LinkedList<GameObject> objectList) {
        x += velX;

        // Nu merge.
        if (x <= 0 || x >= x + 200) objectList.remove(this); // cand atinge extremitatile stanga-dreapta ale ferestrei dispare; il inlaturam din lista de obiecte, altfel raman incarcate in objectList si scade FPS
   // o bataie de 200.. nu cred ca merge, nici cu x >= Game.WIDTH -64; oare de ce
    }

    @Override
    public void render(Graphics g) {
//        g.setColor(Color.PINK);
//        g.fillRect((int)x, (int)y, 16, 16);
            g.drawImage(texture.fireBallImg, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, 16, 16);
    }
}
