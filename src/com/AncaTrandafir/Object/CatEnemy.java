package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.CharacterAnimation;
import com.AncaTrandafir.Window.Camera;

import java.awt.*;
import java.util.LinkedList;

public class CatEnemy extends GameObject {

    private float width =128, height = 64;


    private Texture texture;
    private Camera camera;
    private GameObject player;

    private CharacterAnimation catRunLeftCharacterAnimation, catRunRightCharacterAnimation;


    public CatEnemy(int x, int y, ID id, Texture texture, Camera camera) {           // incarc tipul si textura in constructor, textura returneaza efectiv imaginea blocului
        super(x, y, id);

        this.texture = texture;
        this.camera = camera;

        for (int i=0; i<1; i++)
            for (int j=0; j<12; j++)
                catRunLeftCharacterAnimation = new CharacterAnimation(4, texture.catRunLeft[i]);

        for (int i=0; i<1; i++)
            for (int j=0; j<12; j++)
                catRunRightCharacterAnimation = new CharacterAnimation(4, texture.catRunRight[i]);

        velX = 5;



    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {


        //conditie pt exteriorul camerei
        if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
            ; } else {

            // facem tick doar de ce e in interiorul camerei;

        for (int i=0; i <objectList.size(); i++ )
            if (objectList.get(i).getId() == ID.Player)
                player = objectList.get(i);

            x += velX;

            float diffX = x - player.getX();         //  merge relativ fata de player, il urmeaza

            float distance = (float) Math.sqrt( (x-player.getX()) * (x-player.getX()) + (y-player.getY()) * (y-player.getY()) );

            velX = (float) ((1.3 / distance) * diffX);

            if (x <= 0 || x >= camera.getX() - 64)
                 velX *= -1;   // merge relativ fata de x-ul game-ului, stanga dreapta, face bounce cand atinge capetele ferestrei; nu folosim camera ca nu e statica


            catRunLeftCharacterAnimation.runAnimation();
            catRunRightCharacterAnimation.runAnimation();
        }
    }



    @Override
    public void render(Graphics g) {

            if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
               ; } else {


            if (velX > 0)
                catRunRightCharacterAnimation.drawAnimamation(g, (int) x, (int) y);
            else if (velX < 0)
                catRunLeftCharacterAnimation.drawAnimamation(g, (int) x, (int) y);
            else if (velX == 0)
                g.drawImage(texture.idleCatLeft, (int) x, (int) y, null);


//            g.setColor(Color.RED);
//            g.fillRect((int)x, (int)y, 96, 32 );
        }

            texture.getTextures();

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, (int)width, (int)height);     // sunt dimensiunile caramidei;
                                                              // la fel setam si pt player cu care ne vom intersecta
    }




}
