package com.AncaTrandafir.Object;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Window.Camera;
import com.AncaTrandafir.Window.CharacterAnimation;

import java.awt.*;
import java.util.LinkedList;

public class ChestBonus extends GameObject {

    private float width = 32, height = 32;

    private Texture texture;
    private Camera camera;
    private CharacterAnimation coinSpinAnimation;


    public ChestBonus(int x, int y, ID id, Texture texture, Camera camera) {           // incarc tipul si textura in constructor, textura returneaza efectiv imaginea blocului
        super(x, y, id);
        this.texture = texture;
        this.camera = camera;

    }



    @Override
    public void tick(LinkedList<GameObject> objectList) {
    }


    @Override
    public void render(Graphics g) {

          if ((this.x > -camera.getX() + Game.WIDTH) || (this.x < -camera.getX() - 32) || (this.y > -camera.getY() + Game.HEIGHT) || (this.y < -camera.getY() - 32)) {
                ; } else {

                g.drawImage(texture.coinChestImg, (int) x, (int) y, null);

        }

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, (int)width, (int)height);
                                                              // la fel setam si pt player cu care ne vom intersecta
    }


}
