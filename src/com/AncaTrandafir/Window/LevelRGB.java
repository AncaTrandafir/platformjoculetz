package com.AncaTrandafir.Window;

import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Object.*;

import java.awt.image.BufferedImage;

public class LevelRGB {

    // BufferedImage objects have an upper left corner coordinate of (0, 0)
//    private BufferedImage img, level1, getLevel2RGB;
 //   private BufferedImage   level1SpriteSheet, level2SpriteSheet;
//    private LevelRGB level2RGB;

    private int currentLevel;

    private Handler handler;
    private Texture texture;
    private Camera camera;
    private Player player;
    private HUDEnemy hudEnemy;

    private BufferedImageLoader loader = new BufferedImageLoader();
    private BufferedImage level2SpriteSheet = loader.loadImage("res/level2.png");
//    private  LevelRGB  level2RGB = new LevelRGB(Game.level2SpriteSheet, handler, texture, camera);

    private BufferedImage img;


    public LevelRGB(BufferedImage img, Handler handler, HUDEnemy hudEnemy, Texture texture, Camera camera) {
        this.img = img;
        this.handler = handler;         // injectam handler ca sa putem adauga obiecte tip bricks
        this.texture = texture;
        this.camera = camera;           // injectam camera sa ca facem render de obiectele ce se afla in perimetrul camerei, nu incarcam pe toate pt ca scade fps
        this.hudEnemy = hudEnemy;       // injectam hudEnemy pt ca este in constructorul lui LizardEnemy si vom adauga in lista de obiecte lizard


//        BufferedImageLoader loader = new BufferedImageLoader();

//        level1SpriteSheet = loader.loadImage("res/level.png");
//        LevelRGB level1RGB = new LevelRGB(level1SpriteSheet, handler, texture, camera);
    //    level1RGB.loadImageLevel(level1SpriteSheet);  // returneaza pattern-ul de level

//        level2SpriteSheet = loader.loadImage("res/level2.png");
//        level2RGB = new LevelRGB(level2SpriteSheet, handler, texture, camera);
//    //    level2RGB.loadImageLevel(level2SpriteSheet);  // returneaza pattern-ul de level

    }


    public void loadImageLevel(BufferedImage image){
       int w = image.getWidth();
       int h = image.getHeight();


       for (int i=0; i < w; i++)        // spriteSheet e sub forma de matrice row x col
           for (int j=0; j < h; j++) {

               int pixel = image.getRGB(i, j);      //   getRGB(int x, int y) -> return the value of color pixel at location (x,y).

               int red = (pixel >> 16) & 0xff;      // operator pe biti cu hexadecimal; vezi note jos
               int green = (pixel >> 8) & 0xff;
               int blue = (pixel) & 0xff;

               if (red == 255 && green == 255 && blue == 255)        // culoare alba, pt ca blocurile noastre din levelSpriteSheet sunt albe; tip 0
                   handler.addObject(new Block(i * 32, j * 32,0, ID.Brick, texture, camera));

               if (red == 255 && green == 0 && blue == 0)        // culoare rosie tip 1
                   handler.addObject(new Block(i * 32, j * 32,1, ID.Brick, texture, camera));

               if (red == 0 && green == 255 && blue == 0)        // culoare verde tip 2
                   handler.addObject(new Block(i * 32, j * 32,2, ID.Brick, texture, camera));

               if (red == 100 && green == 100 && blue == 100)   {     // culoare gri tip 3
                   handler.addObject(new Block(i * 32, j * 32,4, ID.Brick, texture, camera));
                   System.out.println("gri incarcat"); }

               if (red == 0 && green == 0 && blue == 255)    {     // culoare blue pt Plyer
                   player = new Player(i * 32, j * 32, ID.Player, handler, texture, camera, this);
                   handler.addObject(player);
                   System.out.println("player incarcat" + player.getX()+" "+player.getY()); }

               if (red == 50 && green == 50 && blue == 50)   {     // culoare gri inchis, FleaEnemy
                   handler.addObject(new FleaEnemy(i * 32, j * 32, ID.FleaEnemy, texture, camera));
                   System.out.println("flea incarcat"); }

               if (red == 150 && green == 150 && blue == 150)   {     // culoare gri deschis, CatEnemy
                   handler.addObject(new CatEnemy(i * 32, j * 32, ID.CatEnemy, texture, camera));
                   System.out.println("cat incarcat"); }

               if (red == 255 && green == 0 && blue == 220)   {     // culoare roz, coin spin
                   handler.addObject(new CoinBonus(i * 32, j * 32, ID.CoinBonus, texture, camera));
                   System.out.println("coin incarcat"); }

               if (red == 255 && green == 135 && blue == 45)   {     // culoare porto, lizard
                   handler.addObject(new LizardEnemy(i * 32, j * 32, ID.LizardEnemy, hudEnemy,  texture, camera, handler));
                   System.out.println("lizard incarcat"); }

               if (red == 255 && green == 216 && blue == 0)   {     // culoare galben, end of level
                   handler.addObject(new EndLevel(i * 32, j * 32, ID.EndLevel, texture));
                   System.out.println("endlevel incarcat"); }
           }

    }


    public void changeLevel() {
        handler.clearLevel();       // sterg toate obiectele
        camera.setX(0);             // aducem camera in pozitie initiala


     //    Game.LEVEL++; // level nu poate fi suprascris.. oare?
//        switch(Game.LEVEL) {  // switch level incepand cu al doilea, pt ca primul level se initializeaza din cls Game.
//            case 1:

        if (Game.LEVEL == 2) {
                loadImageLevel(level2SpriteSheet);  // returneaza pattern-ul de level

                System.out.println("am ajuns la level " + Game.LEVEL); }



    }


}





///////////////////////////////////////
// pixel is an int
//
//an int has 4 bytes in it, each byte has 8 bits in it
//
//example : 0xA3 0x41 0x28 0x76
//
//that's an example of a pixel (1 int  with 4 bytes , 32 bits at all)
//
//Now packed in this pixel is information about the transparency (A) the red, green and blue components.
//
//The transparency is the first byte (0xA3) then red (0x41) then green (0x28) then blue (0x76)
//
//so to get just the red part out, you shift to the right by 16 bits, which gives
//
//0x00 0x00 0xA3 0x41
//
//now the red is the right most spot, but you got that transparency byte there, which isn't good, so you have to remove it
//
//doing & 0xFF, is really
//
//0xA341 & 0x00FF
//
//which means you and each bit, so the bits of A3 are ended with 00 which gives 0, and the bits of 41 are ended with FF which since FF is all ones, gives you the original value, which is 41
//
//so red is
//
//0x00 0x00 0x00 0x41