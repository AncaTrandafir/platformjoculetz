package com.AncaTrandafir.Window;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Object.Player;

public class Camera {

    private float x, y;

    public Camera (float x, float y){
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }



    public void tick(GameObject player){  // Primeste ca parametru un GameObject si va sta fixat pe el
      //  x--;    // de exemplu: doar camera spre muta spre dreapta, nu si obiectele

        x = - player.getX() + Game.WIDTH / 8 ;        // x apartine camerei, si il corelam cu x-ul Player-ului

    }
}
