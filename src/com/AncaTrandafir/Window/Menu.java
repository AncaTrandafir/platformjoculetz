package com.AncaTrandafir.Window;

import com.AncaTrandafir.Framework.AudioPlayer;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Object.EndGameAnimation;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.Random;



public class Menu extends MouseAdapter {

    private Game game;
    private Handler handler;
    private HUD hud;
    private HUDEnemy hudEnemy;
    private  Texture texture;
    private Camera camera;
    private AudioPlayer audioPlayer;

    private BufferedImageLoader loader;
    private BufferedImage level1SpriteSheet;         // sheet -> unde este incarcat level1
    private BufferedImage menuImg, audioImg;


    private Color menuButtonColor1 = Color.BLACK;   // treb pt fiecare buton o variabila, altfel daca schimb culoare la un buton le afecteaza si pe celelalte
    private Color menuButtonColor2 = Color.BLACK;
    private Color menuButtonColor3 = Color.BLACK;
    private Color menuAudioColor = Color.BLACK;

    int xBttn1 = 340, xBttn2 = 340, xBttn3 = 340; // coord x a fiecaruia din cele 3 butoane din meniu; se va schimba la hover
    int widthBttn1 = 300, widthBttn2 = 300, widthBttn3 = 300;   // latimea butonului din meniu; se va schimba la hover


    // variabile pt fasciculul de lumina din Menu
    private float xLight = 100f, yLight = 100f;     // coord
    private float radius = 150f;
    private float velXLight = 3f, velYLight = 3f;   // viteza luminii


    public Menu(Game game, Handler handler, HUD hud, HUDEnemy hudEnemy, Texture texture, Camera camera, AudioPlayer audioPlayer){         // in constructor importam game, handler ca sa importam Menu Animation si HUD ca sa importam hud.getScore pt a afisa scorul la GameOver
        this.game = game;
        this.handler = handler;
        this.hud = hud;
        this.hudEnemy = hudEnemy;
        this.texture = texture;
        this.camera = camera;
        this.audioPlayer = audioPlayer; // aici se afla incarcate sunetele


        loader = new BufferedImageLoader();
        menuImg = loader.loadImage("res/menu.png");
        audioImg = texture.audioImg;


    }

    public void mousePressed(MouseEvent e){
        int mouseX = e.getX();          // coordonatele punctului de click
        int mouseY = e.getY();

        // Play button -> verific ca sunt in starea de meniu, sa nu inurc coordonatele cu alte stari, exemplu Shop

        if (game.gameState == Game.STATE.Menu) {


            if (mouseOver(mouseX, mouseY, 370, 220, 354, 83)) {   // dimensiunile dreptunghiului/butonului de meniu
                System.out.println("apasat butonul de play :)");

                game.gameState = Game.STATE.Game;  // gameState devine Game si cream obiectele apeland LevelRGB

                level1SpriteSheet = loader.loadImage("res/level1.png");
                LevelRGB levelRGB = new LevelRGB(level1SpriteSheet, handler, hudEnemy, texture, camera);
                levelRGB.loadImageLevel(level1SpriteSheet);  // returneaza pattern-ul de level

                AudioPlayer.getSound("menuSound").play();  // incarc sunetul de click

            }
        }


        // Mute the volume
        if (game.gameState == Game.STATE.Menu) {
            if (mouseOver(mouseX, mouseY, Game.WIDTH - 50, Game.HEIGHT -75, 32, 32)) {   // dimensiunile dreptunghiului/butonului audio
                System.out.println("apasat butonul de volum");
                System.out.println(audioPlayer.playing);
                if (audioPlayer.playing == true) {
                    audioPlayer.getMusic("music").pause();
                    audioPlayer.playing = false; // pun pauza si setez pe false, nu mai canta
                } else audioPlayer.getMusic("music").resume();
            }
        }



        // Quit button
        if (game.gameState == Game.STATE.Menu)
          if (mouseOver(mouseX, mouseY, 370, 460, 354, 83)) {   // dimensiunile dreptunghiului/butonului de quit
                System.exit(1);
        }

        // Help button
        if (game.gameState == Game.STATE.Menu)
            if (mouseOver(mouseX, mouseY, 370, 340, 354, 83)) {   // dimensiunile dreptunghiului/butonului de help
                game.gameState = Game.STATE.Help;
                System.out.println("Apasat butonul Help");
                AudioPlayer.getSound("menuSound").play();
        }


        // Back button din Help, care are aceleasi coordonate cu Exit din STATE.Menu
        // Intai verific ca sunt in Help si apoi ca ma aflu in perimetrul respectiv
        if (game.gameState == Game.STATE.Help)
            if (mouseOver(mouseX, mouseY, 410, 580, 150, 35)) {
                System.out.println("apasat butonul de back :)");
                game.gameState = Game.STATE.Menu;  // back la starea de Menu

                AudioPlayer.getSound("menuSound").play();

                return;
        }


        // Play again button din starea de End
        if (game.gameState == Game.STATE.End) {

                if (mouseOver(mouseX, mouseY, 410, 580, 150, 35)) {   // dimensiunile dreptunghiului/butonului de Play Again

                   // handler.clearLevel();

                    for (int i=0; i < handler.objectList.size(); i++)
                        handler.removeObject(handler.objectList.get(i));

                    game.gameState = Game.STATE.Game;
                    HUD.HEALTH = 100;       // reincepem jocul la parametri initiali de health, score, level
                    hud.SCORE = 0;
                    hud.LEVEL = 1;
    //                     for (int i=0; i< handler.objectList.size(); i++) {
    //                         handler.clearMenuAnimation();      // golim de animatiile din End state
    //                         handler.clearEnemies();  // nu ar trebui sa mai fie. dar cateodata ramane un enemy. Bug?
    //                     }
//                    handler.clearLevel();

                    // reincarcam level

                    level1SpriteSheet = loader.loadImage("res/level1.png");
                    LevelRGB levelRGB = new LevelRGB(level1SpriteSheet, handler, hudEnemy, texture, camera);
                    levelRGB.loadImageLevel(level1SpriteSheet);  // returneaza pattern-ul de level

                //    AudioPlayer.getSound("menuSound").play();
            }
        }

    }



    public void mouseReleased (MouseEvent e) {

    }



    public boolean mouseOver(int mouseX, int mouseY, int x, int y, int width, int height) {  // perimetrul butonului de apasat, sa putem apasa oriunde in interior
        if (mouseX > x && mouseX < x + width) {
            if (mouseY > y && mouseY < y + height)
                return true;   // este in spatiul delimitat de cele 4 puncte
            else return false;
        } else return false;

    }



    public void mouseMoved(MouseEvent e){

        int mouseX = e.getX();
        int mouseY = e.getY();


        if (game.gameState == Game.STATE.Menu) {


            if (mouseOver(mouseX, mouseY, 370, 220, 354, 83)) {          // dimensiunile dreptunghiului/butonului de play

                System.out.println("mouse over play button");
                menuButtonColor1 = Color.ORANGE;
                xBttn1 += (270 - xBttn1) * 0.01;
                widthBttn1 += (300 - widthBttn1) * 0.01;

            } else {                                // in tick revenim la dimensiunile initiale ale butonului
                menuButtonColor1 = Color.BLACK;     // doar setam culoarea initiala negru
            }

        }


        // Mute the volume
        if (game.gameState == Game.STATE.Menu) {
            if (mouseOver(mouseX, mouseY, Game.WIDTH - 50, Game.HEIGHT -75, 32, 32)) {   // dimensiunile dreptunghiului/butonului de audio
                menuAudioColor = Color.ORANGE;
            } else menuAudioColor = Color.BLACK;

        }



        // Quit button
        if (game.gameState == Game.STATE.Menu)
            if (mouseOver(mouseX, mouseY, 370, 460, 354, 83)) {   // dimensiunile dreptunghiului/butonului de quit
                menuButtonColor3 = Color.ORANGE;
                xBttn3 += (270 - xBttn3) * 0.01;
                widthBttn3 += (300 - widthBttn3) * 0.01;

            } else {
                menuButtonColor3 = Color.BLACK;
            }


        // Help button
        if (game.gameState == Game.STATE.Menu)
            if (mouseOver(mouseX, mouseY, 370, 340, 354, 83)) {   // dimensiunile dreptunghiului/butonului de help
                menuButtonColor2 = Color.ORANGE;
                xBttn2 += (270 - xBttn2) * 0.01;
                widthBttn2 += (300 - widthBttn2) * 0.01;

            } else {
                menuButtonColor2 = Color.BLACK;
            }



        // Back button din Help, care are aceleasi coordonate cu Exit din STATE.Menu
        // Intai verific ca sunt in Help si apoi ca ma aflu in perimetrul respectiv
        if (game.gameState == Game.STATE.Help)
            if (mouseOver(mouseX, mouseY, 370, 460, 128, 64)) {
                menuButtonColor3 = Color.ORANGE;
            } else {
                menuButtonColor3 = Color.BLACK;
            }



        // Play again button din starea de End
        if (game.gameState == Game.STATE.End)
            if (mouseOver(mouseX, mouseY, 370, 460, 128, 64)) {   // dimensiunile dreptunghiului/butonului de Play Again
                menuButtonColor3 = Color.ORANGE;
                xBttn3 += (270 - xBttn3) * 0.01;
                widthBttn3 += (300 - widthBttn3) * 0.01;

            } else {
                menuButtonColor3 = Color.BLACK;
            }



    }



        // custom color for menu buttons; returnez un GradientPaint

    private RoundRectangle2D gradientButtons(Graphics g, int x, int y, int width, int height) {

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gradient = new GradientPaint(x, y, Color.MAGENTA, 550, y, Color.PINK);
        g2.setPaint(gradient);
        RoundRectangle2D rect = new RoundRectangle2D.Double(x, y, width, height, 20, 20);
        g2.fill(rect);

        return rect;

    }




    public void tick() {

        if (menuButtonColor1 == Color.BLACK) {      // nu e hover pe buton, atunci butonul revine la dimensiunea initiala
            xBttn1 += (370 - xBttn1) * 0.05;
            widthBttn1 += (270 - widthBttn1) * 0.05;
        } else {
            xBttn1 += (270 - xBttn1) * 0.05;
            widthBttn1 += (420 - widthBttn1) * 0.05;
        }

        if (menuButtonColor2 == Color.BLACK) {      // nu e hover pe buton, atunci butonul revine la dimensiunea initiala
            xBttn2 += (370 - xBttn2) * 0.05;
            widthBttn2 += (270 - widthBttn2) * 0.05;
        } else {
            xBttn2 += (270 - xBttn2) * 0.05;
            widthBttn2 += (420 - widthBttn2) * 0.05;
        }

        if (menuButtonColor3 == Color.BLACK) {      // nu e hover pe buton, atunci butonul revine la dimensiunea initiala
            xBttn3 += (370 - xBttn3) * 0.05;
            widthBttn3 += (270 - widthBttn3) * 0.05;
        } else {
            xBttn3 += (270 - xBttn3) * 0.05;
            widthBttn3 += (420 - widthBttn3) * 0.05;
        }


        // setam viteza fasciculului de lumina -----------------------------

        xLight += velXLight;
        yLight += velYLight;

        if (xLight <= 0 || xLight >= Game.WIDTH) velXLight *= -1;  // face bounce de capetele ferestrei
        if (yLight <= 0 || yLight >= Game.HEIGHT) velYLight *= -1;



    }




    public void render(Graphics g) {

        g.drawImage(menuImg, 0, 0, null);  // desenam meniul

        Font font = new Font("MV Boli", Font.PLAIN, 20);
        Font bigFont = new Font("MV Boli", Font.BOLD, 36);
        Font biggerFont = new Font("Forte", Font.BOLD, 72);
        g.setColor(Color.BLACK);

        if (game.gameState == Game.STATE.Menu) {        // creez meniul cu butoane care se redimensioneaza

            g.setFont(biggerFont);
            g.drawString("Run, Henna, run!", Game.WIDTH / 2 - 250, 130);

            gradientButtons(g, xBttn1, 220, widthBttn1, 64);       // creez buton cu culoare gradient, vezi metoda
            g.setColor(menuButtonColor1); // aceasta se va schimba in mouseMoved(); color apartine lui drawRoundRect care face outline-ul butonului
            g.drawRoundRect(xBttn1, 220, widthBttn1, 64, 20, 20);      // vrem ca la hover butonul sa isi schimbe dimensiunea, deci coord x si width vor fi variabile
            g.setFont(bigFont);
            g.setColor(Color.BLACK);
            g.drawString("Play game", Game.WIDTH / 2 - 80 , 262);


            gradientButtons(g, xBttn2, 340, widthBttn2, 64);
            g.setColor(menuButtonColor2);
            g.drawRoundRect(xBttn2, 340, widthBttn2, 64, 20, 20);      // API: ultimii 2 parametri de la drawRoundRect(): width and height of an arc with which to round the corners.
            g.setFont(bigFont);
            g.setColor(Color.BLACK);
            g.drawString("Help / About", Game.WIDTH / 2 - 118, 382);


            gradientButtons(g, xBttn3, 460, widthBttn3, 64);
            g.setColor(menuButtonColor3);
            g.drawRoundRect(xBttn3, 460, widthBttn3, 64, 20, 20);
            g.setFont(bigFont);
            g.setColor(Color.BLACK);
            g.drawString("Quit", Game.WIDTH / 2 - 45, 502);

            g.drawImage(audioImg, Game.WIDTH - 50, Game.HEIGHT - 75, null);      // audio volume


//--------------------------------
            // Adaug fasciculul de lumina, pt starea de Menu

            Graphics2D g2D = (Graphics2D) g;   // Convertim Graphics in Graphics 2D

            Point2D center = new Point2D.Float(xLight, yLight);     // API: The Point2D class defines a point representing a location in (x,y) coordinate space.
            // cream un centru al fasciculului cu coordonatele respective

            float[] intensity = {0.0f, 1.0f}; // un array de float pt intensitatea luminii care ia valori de la 0 la 1
            Color[] colors = {new Color(0.0f, 0.0f, 0.0f, 0.0f), Color.ORANGE};   // array de culori: culoare alba pt lumina si transparenta 100% sa putem vedea prin ea; negru pt fundalul luminii
            RadialGradientPaint paintLight = new RadialGradientPaint(center, radius, intensity, colors);

            // API: The RadialGradientPaint class provides a way to fill a shape with a circular radial color gradient pattern. There can be more than 2 colors.
            //The user must specify the circle controlling the gradient pattern, which is described by a center point and a radius.

            g2D.setPaint(paintLight);
            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));  // blend mode, cu transparenta

            // API: The Composite is used in all drawing methods such as drawImage, drawString, draw and fill . It specifies how new pixels are to be combined with the existing pixels on the graphics device during the rendering process.

            g2D.fillRect(0, 0, 960, 720); // dimensiunile game-ului meu

            g2D.dispose();  // clear cache


            //------------------------------------
        }



        if (game.gameState == Game.STATE.Help) {            // cream starea de HELP

            g.drawImage(game.shopBackground, 200, 50, null);      // instructiuni joc
            g.setFont(bigFont);
            g.drawString("How to play", 303, 150);

            g.setFont(font);
            g.drawString("W to jump", 340, 250);
            g.drawString("Space to shoot", 340, 280);
            g.drawString("Enter to go to shop", 340, 310);
            g.drawString("P to pause the game", 340, 340);

            g.drawString("Graphics attribution: Hyptosis, flaticon.com", 340, 400);

            g.drawRoundRect(410, 580, 150, 35, 20, 20);
            g.drawString("Back", 460, 600);

        }

        if (game.gameState == Game.STATE.End) {            // cream starea de End
            g.drawImage(game.shopBackground, 200, 50, null);
            g.setFont(bigFont);
            g.drawString("Game Over", 400, 150);

            g.setFont(font);
            g.drawString("You lost with a score of " + hud.SCORE + ".", 350, 250);
            g.drawString("You made it to level  " + hud.LEVEL + ".", 350, 300);

            g.setFont(font);

            g.drawRoundRect(410, 580, 150, 35, 20, 20);
            g.drawString("Play again", 460, 600);


        }

    }
}




    // !!!!!NOTE TO SELF:   Why Mouse Adapter and not Mouse Listener

//MouseAdapter:
//
// An abstract adapter class for receiving mouse events. The methods in this class are empty. This class exists as convenience for creating listener objects.
// Extend this class to create a MouseEvent (including drag and motion events) or/and MouseWheelEvent listener and override the methods for the events of interest
//
// In absence of MouseAdapter, if you implement MouseListener, you have to provide implementation to ALL of these interface methods.
//
    //mouseClicked(MouseEvent e)
    //mouseDragged(MouseEvent e)
    //mouseEntered(MouseEvent e)
    //mouseExited(MouseEvent e)
    //mouseMoved(MouseEvent e)
    //mousePressed(MouseEvent e)
    //mouseReleased(MouseEvent e)
    //mouseWheelMoved(MouseWheelEvent e)
// when would it be wise to use the one and when the other ?
//
// If you want to implement all above 8 methods, implement MouseListener.
// If you want to provide implementation for only some of these 8 methods, use MouseAdapter and override only those methods of interest for you.
//
// e.g. If you are interested only in implementing one event ( or few events) like mouseClicked(MouseEvent e) event, best to use MouseAdapter.
// If you implement MouseListener interface in this case, you have to provide blank implementation for other methods, which you are not going to implement.


