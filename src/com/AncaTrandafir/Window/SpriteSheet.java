package com.AncaTrandafir.Window;

import java.awt.image.BufferedImage;

public class SpriteSheet {

    // BufferedImage objects have an upper left corner coordinate of (0, 0)
    private BufferedImage img;

    public SpriteSheet(BufferedImage img) {
        this.img = img;

    }


    public BufferedImage grabImage(int col, int row, int width, int height){   // sparge in blocuri de width si height; ordinea coloana, rand
        BufferedImage image = img.getSubimage((col * width) - width, (row * height) - height, width, height);   // primeste coordonate x,y ale top corner si latime si inaltime
                // transform (x,y) in cod pt a selecta rand si coloana
    return image;

    }


}
