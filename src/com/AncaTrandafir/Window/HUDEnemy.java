package com.AncaTrandafir.Window;

import com.AncaTrandafir.Framework.GameObject;
import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.ID;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import com.AncaTrandafir.Object.ChestBonus;
import com.AncaTrandafir.Object.CoinBonus;

import java.awt.*;

// Heads Up Display - area where players can see their character's vital statistics
// such as current health, attributes, armor level, ammunition count, and more.

public class HUDEnemy {

    public static float healthLizard = 100;   // type float pt ca vom folosi clamp care returneaza float; o apelam din LizardEnemy si Player direct cu cls
    private float greenValue = 255; // verde in RGB, daca R=0, G=255, B=0

    private Handler handler;
    private Camera camera;
    private Texture texture;

    public HUDEnemy(Handler handler, Camera camera, Texture texture) {
        this.handler = handler;         // injectam handler pt ca pozitia barei de sanatate a lizard este relativa de Lizard, care e in lista de obiecte din handler
        // cand ii scade viata lui lizard, treb sa il scoatem din lista de obiecte pe lizard
        this.camera = camera;
        this.texture = texture;         // avem nevoie de texture si camera cand adaugam obiect tip ExtraPoint (are in constructor camera si texture)
    }


    public void tick() {


        // HUD pt life Lizard

        healthLizard = Game.clamp(healthLizard, 0, 100);   // bounds impartit la 2 pt ca health e inmultit cu 2; vezi si Shop -> upgrade life
        greenValue = Game.clamp(greenValue, 0, 255); // o culoare e reprezentata de la 0 la 255
        greenValue = healthLizard * 2;        // health descreste, green creste si in combinatie cu celelalte culori da spre rosu

        greenValue = Game.clamp(greenValue, 0, 255); // o culoare e reprezentata de la 0 la 255
        greenValue = healthLizard * 2;
    }





    public void render(Graphics g) {         // draw the Health Bar; este static, x si y constante, dar este incarcat peste camera si deci se misca odata cu camera

        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.LizardEnemy) {     // iteram lista de obiecte pana ajungem la Lizard


                if (healthLizard > 0) {

                    g.setColor(Color.LIGHT_GRAY);
                    g.fillRect((int) tempObject.getX() + 50, (int) tempObject.getY() - 20, 200, 10);                // set the background HealthBar
                    // pozitie relativa fata de lizard, cu x si y ale lizard
                    g.setColor(new Color(216, (int) greenValue, 78));         // imi creez singura culoarea, cu new Color si cei 3 parametri RGB; green nu e constanta, descreste (in tutorial are R75, B0)
                    g.fillRect((int) tempObject.getX() + 50, (int) tempObject.getY() - 20, (int) healthLizard * 2, 10);         // set the bar for Health

                    g.setColor(Color.WHITE);
                    g.drawRect((int) tempObject.getX() + 50, (int) tempObject.getY() - 20, 200, 10);

                } else {

                    float x = tempObject.getX();       // salvam coordonatele lizard-ului inainte de a-l sterge pt a desena chestCoin cu aceleasi coordonate
                    float y = tempObject.getY();

                    handler.objectList.remove(tempObject); // indepartam lizard si aduagam chest de coins
                    handler.objectList.add(new ChestBonus((int) x, (int) y, ID.ChestBonus, texture, camera));
                    healthLizard = 100; // resetam viata lizard la 100 pt a recrea obiectul pt mai departe; altfel ramane cu life 0 si nu il mai incarca

                }

            }
        }
    }



}
