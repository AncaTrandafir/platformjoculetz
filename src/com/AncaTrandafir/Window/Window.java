package com.AncaTrandafir.Window;

import com.AncaTrandafir.Game;

import javax.swing.*;
import java.awt.*;

public class Window extends Canvas { // to create the window

    // The serialVersionUID is a unique identifier for Serializable classes.
    // It is used during the deserialization of an object, to ensure that a loaded class
    // is compatible with the serialized object.
    // If no matching class is found, an InvalidClassException is thrown.

    private static final  long serialVersionUID = 240840600533728354L;

    public Window (int width, int height, String title, Game game) {    // cream o instanta a Game in interiorul ferestrei
        JFrame frame = new JFrame(title);

        frame.setPreferredSize(new Dimension(width, height));   // dimensiunile ferestrei
        frame.setMaximumSize(new Dimension(width, height));     // nu poate fi redimensionata, altfel apar complicatii
        frame.setMinimumSize(new Dimension(width, height));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   // fereastra se inchide cu X
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);  // se deschide in centrul ecranului, daca nu ar fi s-ar deschide in top left
        frame.add(game);                    // adaugam jocul la frame
        frame.setVisible(true);
        game.start();


    }

}

