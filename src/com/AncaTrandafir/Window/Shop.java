package com.AncaTrandafir.Window;

import com.AncaTrandafir.Framework.Handler;
import com.AncaTrandafir.Framework.Texture;
import com.AncaTrandafir.Game;
import org.w3c.dom.Text;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class Shop extends MouseAdapter {   // reactioneaza la click-ul Mouse-ului

    private Game game;
    private Handler handler;
    private HUD hud;
    private Texture texture;

    private BufferedImage healthImg, boneImg, foodImg, scoreImg;

    private int costSpeed = 100;
    private int costHealth = 500;
    private int costLife = 2000;


    public Shop(Game game, Handler handler, HUD hud, Texture texture){       // upgrade health, upgrade speed, refill health
        this.handler = handler;
        this.hud  =  hud; // incarc hud pt scor
        this.game = game; // incarc game pt STATE
        this.texture = texture;     // aici sunt imaginile pt food, bone etc



        boneImg = texture.boneImg; // os
        foodImg = texture.foodImg; // creste bara
        healthImg = texture.heartImg; // creste health

//        buttonsImg = graphSheet.grabImage(21, 6, 354, 249);
//        scoreImg = graphSheet.grabImage(6, 4, 35, 40);

    }



// !!!!! Conteaza foarte mult sa precizez si starea cand dau click in perimetru, altfel ia coordonatele din celelalte stari (Game, Menu), face bulibaseala

    public void mousePressed(MouseEvent e){
        int mouseX = e.getX();      // coordonatele punctului de click
        int mouseY = e.getY();


        if (game.gameState == Game.STATE.Shop) {            // Verific ca sunt in starea de Shop
            System.out.println("mouse x " + mouseX + "mouse y " + mouseY );
            // Box Health
            if (mouseX >= 420 && mouseX <= 620) {    // sunt dimensiunile randului din tabela de butoane, calculate
                if (mouseY >= 160 && mouseY <= 224) {
                    System.out.println("Box Speed");
                    if (hud.SCORE >= costSpeed) {
                        hud.SCORE = hud.SCORE - costSpeed;
                        costSpeed += 100; // daca vreau sa mai cumpar o data creste valoarea;
                        handler.speed++;      // incrementez viteza, care e setata in handler
                    }
                }
            }

            // Box Health
            if (mouseX >= 420 && mouseX <= 620) {
                if (mouseY >= 250 && mouseY <= 314) {
                    System.out.println("Box Health");
                    if (hud.SCORE >=  costHealth) {
                        hud.SCORE = hud.SCORE - costHealth;
                        costHealth += 300;
                        hud.HEALTH = (100 + (hud.bounds / 2)); // full health
                    }
                }
            }

            // Box Life
            if (mouseX >= 420 && mouseX <= 620) {
                if (mouseY >= 340 && mouseY <= 404) {
                    System.out.println("Box Life");
                    if (hud.SCORE >= costLife) {
                        hud.SCORE = hud.SCORE - costLife;
                        costLife += 700;
                        // health este clamp-uita.. si atunci introducem variabila bounds in cls Hud
                        hud.bounds += 20;  // crestem dreptunghiul de viata cu 20
                        // verdele porneste de la val 100 si, avand in vedere ca health este inmultit cu 2, bounds treb impartit la 2; vezi hud.bounds din Hud (sunt calcule matematice..)
                        hud.HEALTH = (100 + (hud.bounds / 2));
                    }
                }
            }

        }

    }





    public void render(Graphics g) {

        g.drawImage(game.shopBackground,  200, 50, null );

        g.drawImage(boneImg, Game.WIDTH / 2 - 150, 165, null);
        g.drawImage(foodImg, Game.WIDTH / 2 - 150, 255, null);
        g.drawImage(healthImg, Game.WIDTH / 2 - 150, 345, null);

        Font bigFont = new Font("MV Boli", Font.BOLD, 36);
        Font smallFont = new Font("MV Boli", Font.PLAIN, 20);

        g.setColor(Color.BLACK);
        g.setFont(bigFont);
        g.drawString("Shop", Game.WIDTH / 2 - 50, 135);

        g.setFont(smallFont);

        g.drawRoundRect(Game.WIDTH / 2 - 60, 160, 200, 64, 20, 20);
        g.drawString("Upgrade speed", Game.WIDTH / 2 - 30 , 190);
        g.drawString("Cost: " + costSpeed, Game.WIDTH / 2 - 30 , 210);

        g.drawRoundRect(Game.WIDTH / 2 - 60, 250, 200, 64, 20, 20);
        g.drawString("Refill health", Game.WIDTH / 2 - 30, 280);
        g.drawString("Cost: " + costHealth, Game.WIDTH / 2 - 30, 300);

        g.drawRoundRect(Game.WIDTH / 2 - 60, 340, 200, 64, 20, 20);
        g.drawString("Upgrade health", Game.WIDTH / 2 - 30, 370);
        g.drawString("Cost: " + costLife, Game.WIDTH / 2 - 30, 390);

      //  g.drawImage(scoreImg, Game.WIDTH / 2 - 80, 360, null);
        int score = hud.SCORE;
        g.drawString("Your score: " + score, Game.WIDTH / 2 - 30, 455);
    }



}
