package com.AncaTrandafir;

import com.AncaTrandafir.Framework.*;
import com.AncaTrandafir.Object.EndGameAnimation;
import com.AncaTrandafir.Window.*;
import com.AncaTrandafir.Window.Menu;
import com.AncaTrandafir.Window.Window;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Game extends Canvas implements  Runnable {


    private static final  long serialVersionUID = 240840600533728354L;

    public static final int WIDTH = 960, HEIGHT = WIDTH / 12 * 9;            // multiplu de 32 preferabil

    private Thread thread;  // single thread in jocul meu pt ca e simplu

    private boolean running = false;

    private Handler handler;
    private Camera camera;
    private HUD hud;
    private HUDEnemy hudEnemy;
    private Menu menu;
    private Shop shop;
    private AudioPlayer audioPlayer;


//    public static BufferedImage level1SpriteSheet;       // o apelam direct cu clasa

    public BufferedImage shopBackground;

    public static int LEVEL = 1;


    public enum STATE {   // cele trei stari ale jocului nostru, declarate in cls Game
        Menu,
        Game,
        Shop,
        Help,
        End;        // cand se termina viata, se term jocul
    }

    public STATE gameState = STATE.Menu; // we can now pass State like a type;      jocul se deschide cu starea de meniu


    public static boolean paused = false;       // pt a pune pauza jocului
                                                // daca pause=true, in render drawString("Game paused), daca pause=false, in tick() se updateaza
                                                // am keyInput(SpaceBar)



    public Game() {

        BufferedImageLoader loader = new BufferedImageLoader();
        shopBackground = loader.loadImage("res/shop.png"); // o accesez in starile de help si shop

        Texture texture = new Texture();

        camera = new Camera(0, 0);   // coltul stanga sus
        handler = new Handler();
        hud = new HUD();
        hudEnemy = new HUDEnemy(handler, camera, texture);
        shop = new Shop(this, handler, hud, texture);

        audioPlayer = new AudioPlayer();
        audioPlayer.load(); // incarc muzica
        audioPlayer.getMusic("music").loop();  // loop sa se repete in fundal
        audioPlayer.playing = true;   // playing ia valoarea true; cand punem pauza in meniu va fi iar false

        menu = new Menu(this, handler, hud, hudEnemy, texture, camera, audioPlayer);         // mouseListener pt meniu
        this.addMouseListener(menu);
        this.addMouseMotionListener(menu);  // pt menu.mouseMoved()
        this.addMouseListener(shop);

        this.addKeyListener(new KeyInput(this, handler, texture));

        new Window(WIDTH, HEIGHT, "Platform Game - Anca Trandafir", this); // this -> referinta la joc


        // incarc primul nivel in Game, urmand ca celelalte sa se incarce din LevelRGB, cand apelez changeLevel() in Player cand atinge obiectul de endLevele

//        level1SpriteSheet = loader.loadImage("res/level1.png");
//        LevelRGB levelRGB = new LevelRGB(level1SpriteSheet, handler, texture, camera);
//        levelRGB.loadImageLevel(level1SpriteSheet);  // returneaza pattern-ul de level


//


    }



    public void tick(){

//        handler.tick();
//
//
//        for (int i=0; i < handler.objectList.size(); i++) {
//            if (handler.objectList.get(i).getId() == ID.Player)  // va urmari Player
//                camera.tick(handler.objectList.get(i));
//        }
//
//        hud.tick();



        if (gameState == STATE.Game) {

            handler.clearEndGameAnimation(); // inlaturam animatiile din endLevel cand dam replay

            if (!paused) {           // if it's not pause, run everything

                hud.tick();
                handler.tick();

                for (int i = 0; i < handler.objectList.size(); i++) {
                    if (handler.objectList.get(i).getId() == ID.Player)  // va urmari Player
                        camera.tick(handler.objectList.get(i));
                }


                if (HUD.HEALTH <= 0)        // daca Health scade sub 0, intra in starea de End; Render pt End se face mai jos si mai este cod in cosntructor Game() pt End
                {
                    gameState = Game.STATE.End;  // pt a face render de graphics din cls Menu, stare End
                    // for (int i = 0; i < handler.objectList.size(); i++) {   // golim ecranul de inamici si player
                    handler.clearLevel();

                    Random r = new Random();
                    for (int i = 0; i < 20; i++) {
                        handler.addObject(new EndGameAnimation(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.EndGameAnimation, handler));  // adaugam animatii
                    }
                }
            }


        } else if (gameState == STATE.Menu || gameState == STATE.End) {
            menu.tick();
            handler.tick();



    }



        }







    public void render(){            // Canvas will be updating many times a second so I need a buffer strategy; Fara buffer strategy am un FPS enorm
                                     // Pentru cursivitate intre frame-uri. Ele sunt gata si pregatite sa fie incarcate

        BufferStrategy bs  = this.getBufferStrategy();  // this se referea de fapt la clasa Canvas pe care o extindem; getBufferStrategy e o metoda a Canvas

        if (bs == null) {   // pt ca la initializare este null; cand nu mai este nu mai cream. pt ca nu vrem sa cream BufferedStrategy de fiecare data cand apelam render si o apelam de f multe ori
            this.createBufferStrategy(3);   // recomandat 3 buffere, 3 layere
            return;
        }

        Graphics g = bs.getDrawGraphics();
        Graphics2D g2d = (Graphics2D) g;



        if (gameState == STATE.Game) {

            BufferedImageLoader loader = new BufferedImageLoader();  // cream o instanta de BuffredImageLoader, iar cu metoda load(), incarcam fisierul de la path
            BufferedImage background = loader.loadImage("res/background.png");
            g.drawImage(background, 0, 0, null);


            g2d.translate(camera.getX(), camera.getY());  // Begin of camera;   g2d se translateaza la coordonatele camerei

            g.drawImage(background, 0, 0, null);        // background in camera ca sa nu fie static
            handler.render(g);

            g2d.translate(-camera.getX(), -camera.getY());  // end of camera; anything in between is gonna be affected by camera: handler.render(g)

            hud.render(g);      // HUD pus peste camera si se translateaza odata cu camera; daca il pun in camera ramane in urma, in pozitie initiala


        }

        else if (gameState == STATE.Shop) {
            shop.render(g);  // in starea de shop nu mai face render de obiecte; ele sunt incarcate in handler, dar nu le vrem
        }




        else if (gameState == STATE.Menu || gameState == STATE.Help || gameState == STATE.End) {  // Daca e starea de Menu sau Help, imi apeleaza cls Menu, pt ca si Help e facut in Menu
            menu.render(g);
            handler.render(g);  // MenuAnimations sunt continute aici in faza asta
        }


        if (paused) {           // pus dupa ce se incarca starea de game ca sa se incarce scris peste game
            g.setColor(Color.ORANGE);
            g.setFont(new Font("MV Boli", Font.BOLD, 72));
            g.drawString("Game paused", Game.WIDTH / 2 - 200 , Game.HEIGHT / 2);
        }


        // Each Graphics object uses system resources, and these should be released.
        // Like needing to call close() on an InputStream that you have opened.
        g.dispose();
        bs.show();
    }


    public synchronized void start(){           // synchronized has to do with threads
        if (running)            // safe measure: it the start method is called again, it's goona now the thread is already running so it won't create another one
            return;             // cu if-ul asta nu mai e nevoie de stop thread, vezi littleGame

        running = true;
        thread = new Thread(this);
        thread.start();
    }


    @Override
    public void run() {             // The heart of the Game: Game Loop
        this.requestFocus();    // Sa nu mai fie nevoie de sa fac click pe fereastra ca sa putem avea ctrl asupra jocului cu tastele

        // Game loop - the heartbeat of the game, to make it actually work
        // Se copiaza efectiv codul asta, e popular
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;   // it's running at 60 ticks, meaning updates, per second
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1) {
                tick();
                delta--;
            }
            if (running)
                render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println("FPS " + frames);        // numarul de FPS
                frames = 0;
            }
        }
    }



    // Metoda pentru a seta limite pt variabila var
    // Lucram cu float pt ca o vom folosi si pt coordonatele x, y care sunt float
    public static float clamp(float var, float min, float max) {
        if (var >= max)
            return var = max;   // nu poate depasi max sau min
        else if (var <= min)
            return var = min;
        else return var;
    }



    public static void main(String[] args) {
        new Game();
    }

}
